import React from 'react';
import ReactDOM from 'react-dom';
import Table from './table';

document.addEventListener('DOMContentLoaded', function(){
    ReactDOM.render(
        React.createElement(Table),
        document.getElementById('app')
    );
});
