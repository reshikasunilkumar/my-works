import React,{Component} from 'react';
class Table extends Component {
	constructor(){
		super();
		this.state = {
			row: 0,
			col: 0,
			resTable: []
		};
	}
	
	createTable() {
		const { row, col } = this.state;
		const resTable = [];
		for (let i = 0; i < row; i++) {
			const resCol = [];
			for (let j = 0; j < col; j++) {
				let c = i*j%256;
				let tableStyle = {
					width:'70px',
					height:'60px',
					backgroundColor: `rgb(${c},${c},${c})`,
					margin:'10px'
				};
				resCol.push(<td style={tableStyle}>
					
					</td>)
			}
			resTable.push(
				<tr>
					{resCol}
				</tr>
			);
		}
		this.setState({
			resTable
		});
	}

	handleRowChange(event) {
		this.setState({
			row: event.target.value
		});
	}

	handleColChange(event) {
		this.setState({
			col: event.target.value
		});
	}

    render(){
		const styles = {
			buttonStyle: {
				backgroundColor: '#01D069',
				padding: '10px',
				marginLeft: '10px',
			},
			inputStyle: {
				padding: '10px',
				boxShadow: '0 2px 2px #ccc',
				margin: '10px'
			}
		}
		
		
       	return(
         	<div>
				<input type="text" id="row" onChange={this.handleRowChange.bind(this)} placeholder="Enter number of rows" style={styles.inputStyle}></input>
				<input type="text" id="col" onChange={this.handleColChange.bind(this)} placeholder="Enter number of columns" style={styles.inputStyle}></input>
				<button onClick={this.createTable.bind(this)} style={styles.buttonStyle}>
					Create Table
				</button>
				<table border="1">
					{this.state.resTable}
				</table>
        	</div> 

       );
   }
}
export default Table;