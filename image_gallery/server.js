var express = require('express');
var app = express();

//set the view engine to ejs
app.set("view engine","ejs");

//index page
app.get('/',function(req,res){
    const imgArray = [];
    for(var i=0;i<12;i++){
        var sizes = [200,250,300,350,400];
        //Picking random width & height
        var rWidth = sizes[Math.floor(Math.random() * sizes.length)];
        var rHeight = sizes[Math.floor(Math.random() * sizes.length)];
        //Image Array
        imgArray.push({
            url:`https://loremflickr.com/${rWidth}/${rHeight}`,
        });   
    }
    res.render('index',{imgArray});
    
});



app.listen(50001);
console.log('App is listening at port 50001');

