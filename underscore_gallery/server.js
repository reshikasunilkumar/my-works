var express = require('express');
var path = require('path');
var app = express();

//set the view engine to ejs
app.set("view engine","ejs");
app.use(express.static(path.join(__dirname, 'public')));

app.get('/api/v1/image-list',function(req,res){
    const promise = createImages();
    promise.then((resp) => {
        res.json({
            success: true,
            data: resp
        });
    });  
});

app.get('/jquery',function(req,res){
    res.render('index.ejs'); 
});
app.get('/',function(req,res){
    res.render('main.ejs');
});

function createImages(){   
    return new Promise((resolve,reject) => {
        const imgArray = [];
        for(var i=0;i<12;i++){
            var sizes = [200,250,300,350,400];
            //Picking random width & height
            var rWidth = sizes[Math.floor(Math.random() * sizes.length)];
            var rHeight = sizes[Math.floor(Math.random() * sizes.length)];
            //Image Array
            imgArray.push({
                url:`https://loremflickr.com/${rWidth}/${rHeight}`,
            });   
        }
        resolve(imgArray);
    });
}
app.listen(50001);
console.log('App is listening at port 50001');

